<?php if (get_the_ID() !== 41) : ?>

<!-- .COLLAGE -->
<div class="row">
    <div class="col-xs-12">
        <div class="collage collage-footer">
            <div class="elliot">
                <?php $image = get_field('box_3_image', 'options') ?>
                <div class="img" style="background-image: url(<?php print $image['url'] ?>)"></div>
            </div>
            <div class="looking-for-advice">
                <?php the_field('box_4_content', 'options') ?>
            </div>
            <div class="outlink">
                <div onclick="document.location='<?php the_field('box_1_url', 'options') ?>';">
                    <a href="<?php the_field('box_1_url', 'options') ?>">
                        <?php the_field('box_1_content', 'options') ?>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div onclick="document.location='<?php the_field('box_2_url', 'options') ?>';">
                    <a href="<?php the_field('box_2_url', 'options') ?>">
                        <?php the_field('box_2_content', 'options') ?>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="outlink">
                <div onclick="document.location='<?php the_field('box_5_url', 'options') ?>';">
                    <a href="<?php the_field('box_5_url', 'options') ?>">
                        <?php the_field('box_5_content', 'options') ?>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
                <div onclick="document.location='<?php the_field('box_6_url', 'options') ?>';">
                    <a href="<?php the_field('box_6_url', 'options') ?>">
                        <?php the_field('box_6_content', 'options') ?>
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.COLLAGE -->

<?php endif ?>