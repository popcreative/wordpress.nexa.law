<!-- #HEADER -->
<div class="container"  id="header">
    <div class="row">
        <div class="col-xs-12 col-sm-10">
            <ul class="list-inline">
                <?php
                    wp_nav_menu(array(
                        'menu' => 'Primary',
                        'fallback_cb' => false,
                        'items_wrap' => '%3$s',
                        'container' => false
                    ))
                ?>
                <?php
                    print str_replace('menu-item ', 'menu-item hidden-lg hidden-md ', wp_nav_menu(array(
                        'menu' => 'Secondary',
                        'fallback_cb' => false,
                        'items_wrap' => '%3$s',
                        'container' => false,
                        'echo' => false
                    )))
                ?>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-2">
            <a href="#toggle-navigation" class="btn btn-square toggle-nav"><i class="fa fa-bars"></i></a>
            <a class="emblem" href="<?php echo home_url() ?>"><img src="<?php echo asset('img/emblem.svg') ?>"></a>
            <a href="tel: <?php echo str_replace(' ', '', get_field('telephone', 'options')) ?>" class="btn btn-square telephone"><span class="hidden-sm hidden-md hidden-lg"><i class='fa fa-phone'></i></span><span class="hidden-xs"><?php the_field('telephone', 'options') ?></span></a>
        </div>
    </div>
</div>
<!-- /#HEADER -->