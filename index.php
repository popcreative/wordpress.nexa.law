<?php get_header(); ?>

<?php while (have_posts()) : the_post() ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-5">
        <h1><?php the_title() ?></h1>
    </div>
    <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>

<?php
    $content_index = 0;
    while (have_rows('content')) : the_row();
        include('flexible/'.get_row_layout().".php");
        $content_index++;
    endwhile;
?>

<?php endwhile ?>

<?php get_footer(); ?>