<?php
error_reporting(0);
@ini_set('display_errors', 0);

include "./vendor/autoload.php";

add_theme_support('post-thumbnails', array('post'));

function asset($file = false)
{
    return get_template_directory_uri() . '/assets/' . $file;
}

add_action('after_setup_theme', 'register_menus');
function register_menus()
{
    register_nav_menus(array(
        'primary' => 'Primary Menu',
        'secondary' => 'Secondary Menu',
        'main-1' => 'Main Links 1',
        'main-2' => 'Main Links 2',
        'policy' => 'Website Policy',
        'main-contacts' => 'Main Contacts',

    ));
}

acf_add_options_page();

function my_add_editor_styles()
{
    add_editor_style(asset('css/wysiwyg.css') . "?t=" . time());
}
add_action('admin_init', 'my_add_editor_styles');

function make_component_class($index)
{
    return 'component_index_'.$index;
}

function make_column_classes($input)
{
    $return = "";
    $columns = explode(",", $input);
    $classes = array("xs", "sm", "md", "lg");
    foreach ($columns as $index => $column) {
        $column = trim($column);
        $return .= " col-{$classes[$index]}-{$column} ";
    }

    return $return;
}

//* Set default Attachment Display Settings
function attachment_display_defaults_setup() {
	update_option('image_default_align', 'none' );
	update_option('image_default_link_type', 'none' );
	update_option('image_default_size', 'large' );
}

add_action('after_setup_theme', 'attachment_display_defaults_setup');

// Team custom post type
function team_members() {
    register_post_type( 'meet-the-team',
        array(
            'labels' => array(
                'name' => __( 'Team Members' ),
                'singular_name' => __( 'Team member' ),
                'add_new_item' => __('Add new team member'),
                'edit_item' => __('Edit team member'),
                'new_item' => __('New team member'),
                'view_item' => __('View team member'),
                'search_items' => __('Search team members')
            ),
            'show_in_menu' => true,
            'menu_position' => 6,
            'public' => true,
            'has_archive' => true,
            'menu_icon' => 'dashicons-groups'
        )
    );
}

add_action( 'init', 'team_members' );

add_shortcode('calculator', function($atts)
{
    return get_template_part('calculator');
});
