<?php get_header(); ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-4 logo">
        <a href="<?php echo home_url() ?>"><img src="<?php echo asset('img/logo.svg') ?>"></a>
    </div>
    <div class="col-xs-12 col-md-8 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>

<?php
    $background = get_field('hero_background');
    $mobile = get_field('hero_mobile_background');
    $tablet = get_field('tablet_background');
?>

<style>
    .hero.hero-home {
        background-image: url(<?php echo $background['url'] ?>)
    }

    @media (max-width: 1200px) {
        .hero.hero-home {
            background-image: url(<?php echo $tablet['url'] ?>);
            background-position: center center;
            padding: 2rem;
        }
    }

    @media (max-width: 768px) {
        .hero.hero-home {
            background-image: url(<?php echo $mobile['url'] ?>);
            background-position: center center;
            padding: 2rem;
        }

        .component {
            padding-left: 15px;
            padding-right: 15px;
        }

        .hero.hero-home h2  {
            font-size: 42px;
        }
    }
</style>

<!-- .HERO -->
<div class="row">
    <div class="col-xs-12">
        <div class="hero hero-home">
            <h2><?php the_field('hero_title') ?></h2>
            <p>
                <?php the_field('hero_content') ?>
            </p>
        </div>
    </div>
</div>
<!-- /.HERO -->

<div class="col-xs-12">
    <div class="row">
        <div class="row content">
            <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <div class="component" style="padding-top: 0; padding-bottom: 0;">
                    <h1><?php the_field('lower_title') ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="background: #f5f5f5">
        <div class="row content">
            <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <div class="component">
                    <?php the_field('lower_content') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>