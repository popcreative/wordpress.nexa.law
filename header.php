<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php wp_title() ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://use.typekit.net/vap0kbu.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <?php

            wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
            wp_enqueue_style('aos', '//cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css');   
            wp_enqueue_style('app', asset('css/app.css'));
            wp_enqueue_style('style', get_stylesheet_uri());

            wp_enqueue_script('app', asset('js/main.js'), array(), false, true );
            wp_enqueue_script('aos', '//cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js', array(), false, true ); 
            
            wp_head();

        ?>

        <!-- Google Tag Manager - Top of head -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PGWJRH8');</script>
        <!-- End Google Tag Manager -->

        <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "LocalBusiness",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "Office Suite 7a, 13 Church Street",
                "addressLocality": "Oswestry",
                "addressRegion": "Shropshire",
                "postalCode": "SY11 2SU",
                "addressCountry": "England"
            },
            "hasMap": "https://goo.gl/maps/KM89gRK3qZ42",
            "contactPoint": {
                "@type": "ContactPoint",
                "contactType": "customer service",
                "telephone": "+44330242420",
                "email": "info@nexa.law"
            }
        }
	    </script>

        <style type="text/css">
            /* Calculator page */
            @media(min-width: 768px) {
                .page-173 table tr td:first-child {
                    position: relative;
                }

                .page-173 table tr td:first-child:after {
                    content: url('https://www.nexa.law/wp-content/themes/nexa/assets/img/grey-right-arrow.png');
                    position: absolute;
                    right: 30%;
                    opacity: 0.85;
                }

                .mr-sm-10 {
                    margin-right: 10px;
                }
            }

            .btn-blue {
                background-color: #2d87c0;
                color: #FFF !important;
                -webkit-transition: all .15s;
                transition: all .15s;
                padding: 10px 30px;
            }

            .btn-blue:hover,
            .btn-blue:focus {
                background-color: #156da5;
                color: #FFF !important;
            }

            @media(max-width: 767px) {
                .btn.btn-contact-us-today {
                    padding: 10px 30px !important;
                }

                .btn + .btn {
                    margin-top: 15px;
                }

                .btn-center-mob {
                    display: block;
                    max-width: 350px;
                    margin: 0 auto;
                }
            }
        </style>

    </head>
    <body class="<?php if(is_front_page()){ echo 'front-page'; } ?> page-<?php the_ID() ?>">

        <!-- Google Tag Manager (noscript) - opening Body -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGWJRH8"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <?php get_template_part('partials/_header') ?>

        <div class="container">