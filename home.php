<?php get_header(); ?>
<div class="row" id="title">
    <div class="col-xs-12 col-md-5">
        <h1>Newsfeed</h1>
    </div>
    <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>
<?php 
    $is_first_couple_posts = true;  
    $counter = 1;
?>
<div class="row flex-sm-wrap">
    <?php while (have_posts()) : the_post() ?>
        <?php 
            $is_first_couple_posts = ($counter <= 2); 
            $background_url = asset('img/news-placeholder.png');
                    
            if( has_post_thumbnail() ){
                $background_url = get_the_post_thumbnail_url(get_the_ID(), 'large');
            }
        ?>
        <style type="text/css">
            .single-blog {
                min-height: 330px;
            }
            @media(min-width: 768px) {
                .flex-sm-wrap {
                    display: -webkit-box;
                    display: -webkit-flex;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-flex-wrap: wrap;
                        -ms-flex-wrap: wrap;
                            flex-wrap: wrap;
                }
            }
        </style>
        <div class="single-blog col-xs-12 <?php echo ($is_first_couple_posts) ? "col-md-6" : "col-sm-12 col-md-4" ?>">
            <a href="<?php the_permalink() ?>">
                <div class='post-thumbnail'>
                    <img src="<?php echo $background_url ?>" class="img-responsive">
                </div>
            </a>
            <h2 class="h4"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
            <p class="muted">
                Posted <?php the_time("jS M Y") ?>
            </p>
        </div>
        <?php $counter++ ?>
    <?php endwhile ?>
</div>

<div class="text-center">
    <?php posts_nav_link() ?>
</div>

<?php get_footer(); ?>