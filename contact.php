<?php

// Template Name: Contact
get_header();

?>

<?php while(have_posts()): the_post() ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-3">
        <h1><?php the_title() ?></h1>
    </div>
    <div class="col-xs-12 col-md-9 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>

<div class="component two_columns
    border-top        border-between    component_index_0">

    <style>
            </style>

    <div class="row">
        <div class=" col-xs-12 col-md-3">
            <div class="left">
                <?php the_content() ?>
            </div>
        </div>
        <div class=" col-xs-12 col-md-9">
            <div class="right">

                <form action="#" method="post">

                    <?php

                        $isValid = false;
                        $isSubmitted = isset($_POST['form']);

                        if ($isSubmitted) {
                            $templater = new \kevdotbadger\Templater\Renderer;
                            $validator = new kevdotbadger\Validator\Validator;
                            $validator->addRule('first_name')
                                ->notEmpty(array('message' => "First Name must not be empty"));
                            $validator->addRule('last_name')
                                ->notEmpty(array('message' => "Last Name must not be empty"));
                            $validator->addRule('email')
                                ->isEmail(array('message' => "Email Address must be a valid email address"));
                            $validator->addRule('enquiry')
                                ->notEmpty(array('message' => "Enquiry must not be empty"));

                            $isValid = $validator->validate($_POST);

                            if ($isValid) {
                                $mailer = new \kevdotbadger\StupidMailer\Mailer;
                                $mailer->setFrom(array('website@nexa.law' => 'Nexa Law'));
                                $mailer->setSubject("Contact Form Enquiry");
                                $mailer->setRecipients(
                                    array(
                                        'info@nexa.law' => 'Nexa Law',
                                        'eliot.hibbert@nexa.law' => 'Eliot Hibbert',
                                        'forms@popcreative.co.uk'  => 'Pop Creative',
                                        //'timord@popcreative.co.uk' => 'Tim Ord'
                                    )
                                );

                                $mailer->addContent(trim(stripslashes(
                                    $templater->fill("
                                    Hello,

                                    A user has filled in the contact form, their details are below:

                                    First Name: {first_name}\n
                                    Last Name: {last_name}\n
                                    Email: {email}\n
                                    Enquiry: {enquiry}\n

                                    ")->with($_POST)->render()
                                )));

                                $mailer->send();

                                // Send to Zoho
                                $client = new \Kevdotbadger\Zoho\Lead('e3ad0bd5da28020375bfeb51be08e3a9', 'eu');
                                $payload = [
                                    'Lead Source'   => 'Web Lead - Contact Form',
                                    'Lead Status'   => 'Open',
                                    'Enquiry'       => \kevdotbadger\Input\Input::get('enquiry'),
                                    'First Name'    => \kevdotbadger\Input\Input::get('first_name'), // needed
                                    'Last Name'     => \kevdotbadger\Input\Input::get('last_name'), // needed
                                    'Email'         => \kevdotbadger\Input\Input::get('email'),
                                ];
                                $client->create($payload);

                            }
                        }
                    ?>

                    <?php if ($isSubmitted) : ?>

                        <?php if ($isValid) : ?>

                            <div class="alert alert-success">
                                <p><i class="fa fa-check-circle" aria-hidden="true"></i> Thank you, the request has been sent to Nexa Law.</p>
                            </div>

                        <?php else : ?>

                            <div class="alert alert-danger">
                                <p><i class="fa fa-times-circle" aria-hidden="true"></i> We're sorry but the form failed to send. Please try again.</p>
                                <ul>
                                    <?php foreach ($validator->getErrorMessages() as $error) : ?>
                                        <li><?php echo $error ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>

                        <?php endif ?>

                    <?php endif ?>

                    <div class="row">
                        <div class="col-xs-12 col-sm-6" style="border: 0">
                            <div class="form-group">
                                <label for="first_name">First Name<sup>*</sup></label>
                                <input type="text" name="first_name" placeholder="First Name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="first_name">Last Name<sup>*</sup></label>
                                <input type="text" name="last_name" placeholder="Last Name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Email Address<sup>*</sup></label>
                                <input type="text" name="email" placeholder="Email Address" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label for="enquiry">Enquiry<sup>*</sup></label>
                                <textarea type="text" rows=8 name="enquiry" placeholder="Enquiry" class="form-control" required></textarea>
                            </div>
                            <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block btn-wide">Send</button>
                                </div>
                        </div>
                    </div>

                    <input type="hidden" name="form" value="form">

                </form>

            </div>
        </div>
    </div>
</div>


<?php endwhile ?>

<?php get_footer(); ?>