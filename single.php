<?php get_header(); ?>

<?php while (have_posts()) : the_post() ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-5">
        <a class="back-to-blog-link" href="https://www.nexa.law/newsfeed">
            <i class="fa fa-chevron-left"></i> Back to newsfeed
        </a>
    </div>
    <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
        <ul class="list-inline" style="margin-top: 4px;">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>
<div class="component border-top">
    <div class="blog-body-wrapper">
        <h1 class="text-center"><?php the_title() ?></h1>
        <?php get_template_part('partials/social-media-sharing-bar') ?>
        <?php the_content() ?>
        <?php get_template_part('partials/social-media-sharing-bar') ?>
    </div>
</div>
<?php endwhile ?>

<?php get_footer(); ?>