<?php

// Template Name: Newsletter Request
get_header();

?>

<?php while (have_posts()) : the_post() ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-5">
        <h1><?php the_title() ?></h1>
    </div>
    <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false,
                    'menu' => 'Secondary',
                ))
            ?>
        </ul>
    </div>
</div>

<div class="component two_columns border-top border-between component_index_0">
    <div class="row">
        <div class=" col-xs-12 col-md-6 hidden-sm hidden-xs">
            <div class="left image">

                <?php while (have_rows('brochure_details')) : the_row() ?>

                    <?php $image = get_sub_field('image') ?>

                    <img src="<?php echo $image['url'] ?>">

                <?php endwhile ?>

            </div>
        </div>
        <div class=" col-xs-12 col-md-6">
            <div class="right">
                <form action="#" method="post">
                    <p>
                        Please enter your details below to request one or both of our brochures.
                    </p>

                    <?php

                        $isValid = false;
                        $isSubmitted = isset($_POST['form']);

                        if ($isSubmitted) {
                            $templater = new \kevdotbadger\Templater\Renderer;
                            $validator = new kevdotbadger\Validator\Validator;
                            $validator->addRule('first_name')
                                ->notEmpty(array('message' => "First Name must not be empty"));
                            $validator->addRule('last_name')
                                ->notEmpty(array('message' => "Last Name must not be empty"));
                            $validator->addRule('email')
                                ->isEmail(array('message' => "Email Address must be a valid email address"));

                            $isValid = $validator->validate($_POST);

                            if ($isValid) {
                                $mailer = new \kevdotbadger\StupidMailer\Mailer;
                                $mailer->setFrom(array('website@nexa.law' => 'Nexa Law'));
                                $mailer->setSubject("Newsletter Request");
                                $mailer->setRecipients(
                                    array(
                                        'forms@popcreative.co.uk'  => 'Pop Creative',
                                        'info@nexa.law' => 'Nexa Law',
                                        'eliot.hibbert@nexa.law' => 'Eliot Hibbert',
                                        //'timord@popcreative.co.uk' => 'Tim Ord'
                                    )
                                );

                                $mailer->addContent(trim(stripslashes(
                                    $templater->fill("
                                    Hello,

                                    A user has requested a brochure, their details are below:

                                    First Name: {first_name}\n
                                    Last Name: {last_name}\n
                                    Brochure Type: {brochure_type}\n
                                    Email: {email}\n

                                    ")->with($_POST)->render()
                                )));

                                $mailer->send();

                                // Send to Zoho
                                $client = new \Kevdotbadger\Zoho\Lead('e3ad0bd5da28020375bfeb51be08e3a9', 'eu');
                                $payload = [
                                    'Lead Source'       => 'Web Lead - Brochure Request Form',
                                    'Lead Status'       => 'Open',
                                    'Brochure Selected' => \kevdotbadger\Input\Input::get('brochure_type'),
                                    'First Name'        => \kevdotbadger\Input\Input::get('first_name'), // needed
                                    'Last Name'         => \kevdotbadger\Input\Input::get('last_name'), // needed
                                    'Email'             => \kevdotbadger\Input\Input::get('email'),
                                ];
                                $client->create($payload);

                            }
                        }
                    ?>

                    <?php if ($isSubmitted) : ?>

                        <?php if ($isValid) : ?>

                            <div class="alert alert-success">
                                <p><i class="fa fa-check-circle" aria-hidden="true"></i> Thank you, the request has been sent to Nexa Law.</p>
                            </div>

                        <?php else : ?>

                            <div class="alert alert-danger">
                                <p><i class="fa fa-times-circle" aria-hidden="true"></i> We're sorry but the form failed to send. Please try again.</p>
                                <ul>

                                    <?php foreach ($validator->getErrorMessages() as $error) : ?>
                                        <li><?php echo $error ?></li>
                                    <?php endforeach ?>

                                </ul>
                            </div>

                        <?php endif ?>

                    <?php endif ?>

                    <div class="row">
                        <div class="col-xs-12" style="border: 0">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">First Name<sup>*</sup></label>
                                        <input type="text" name="first_name" placeholder="First Name" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="first_name">Last Name<sup>*</sup></label>
                                        <input type="text" name="last_name" placeholder="Last Name" class="form-control" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="type">Brochure<sup>*</sup></label>
                                <select name="brochure_type" placeholder="What type of brochure would you like?" class="form-control" required>
                                    <option value="corporate_brochure">Corporate brochure</option>
                                    <option value="becoming_a_consultant_lawyer_brochure">Becoming a consultant lawyer brochure</option>
                                    <option value="both">Both</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="email">Email Address<sup>*</sup></label>
                                <input type="text" name="email" placeholder="Email Address" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block btn-wide">SEND REQUEST</button>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="form" value="form">

                </form>

                <div class="brochure_points">

                    <?php while (have_rows('brochure_details')) : the_row(); ?>

                        <div class="point">
                            <h3><?php the_sub_field('title') ?></h3>
                            <strong>includes:</strong>
                            <ul>

                                <?php while (have_rows('points')) : the_row(); ?>
                                    <li><?php the_sub_field('point') ?></li>
                                <?php endwhile ?>

                            </ul>
                        </div>

                    <?php endwhile ?>

                </div>
            </div>
        </div>
    </div>
</div>


<?php endwhile ?>

<?php get_footer(); ?>