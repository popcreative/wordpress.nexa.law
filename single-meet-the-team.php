<?php get_header(); ?>

<?php while (have_posts()) : the_post() ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-6">
        <h1><?php the_title() ?><?php echo (get_field('team_job_title')) ? ' <small class="job-title"><span>-</span> ' . get_field('team_job_title') . '</small>' : '' ?></h1>
    </div>
    <div class="col-xs-12 col-md-6 hidden-xs hidden-sm">
        <ul class="list-inline" style="margin-top: 4px;">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>
<div class="component border-top">
    <div class="team-body-wrapper">
        <div class="row">
            <div class="col-xs-12 col-md-5">

                <style>
                    #title h1 small {
                        color: #535353;
                    }

                    .team-body-wrapper {
                        max-width: 1140px;
                        margin: 0 auto;
                    }

                    .team-images > img {
                        margin: 0 auto 30px auto;
                    }

                    .team-images .thumbnails:before, 
                    .thumbnails:after {
                        content: "";
                        display: table;
                        clear: both;
                    }

                    .team-images .thumbnails .image {
                        display: inline-block;
                        width: 50%;
                        float: left;
                    }

                    .thumbnails .image:nth-child(odd) {
                        padding-right: 5px;
                    }

                    .thumbnails .image:nth-child(even) {
                        padding-left: 5px;
                    }

                    @media(max-width: 991px) {
                        .team-images .thumbnails {
                            display: none;
                        }
                    }

                    @media(max-width: 767px) {
                        #title h1 small.job-title {
                            display: block;
                            margin-top: 5px;
                        }

                        #title h1 small.job-title span {
                            display: none;
                        }
                    }

                    @media(min-width: 992px) {
                        .team-images {
                            display: inline-block;
                            max-width: 388px;
                        }
                        .team-images > img {
                            margin: 0 0 10px 0;
                        }
                    }

                    .team-content .contact-info {
                        padding: 10px;
                        border-top: 1px solid #CCC;
                        border-bottom: 1px solid #CCC;
                        margin-bottom: 15px;
                    }

                    .team-content .contact-info span {
                        display: block;
                    }

                    .team-content .contact-info a {
                        color: #535353;
                        font-weight: normal;
                        transition: color 0.2s ease;
                    }

                    .team-content .contact-info a:hover,
                    .team-content .contact-info a:focus {
                        color: #76A92B;
                    }

                    .team-content .social-links a.linkedin-link {
                        color: #1585af;
                    }

                    .team-content .social-links a.linkedin-link:hover {
                        color: #086386;
                    }

                    @media(min-width: 480px) {
                        .team-content .contact-info span {
                            display: inline-block;
                        }

                        .team-content .contact-info span:first-child {
                            margin-right: 30px;
                        }
                    }
                </style>

                <div class="team-images">
                    <?php 
                        $images = get_field('team_imgs');
                        $featuredImage = array_shift($images);
                    ?>
                    <img class="img-responsive" src="<?php echo $featuredImage['url'] ?>" data-aos="fade">
                    
                    <div class="thumbnails">
                        <?php foreach($images as $image): ?>
                            <div class="image">
                                <img class="img-responsive" src="<?php echo $image['url'] ?>" data-aos="fade">
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-7">
                <div class="team-content">
                   <?php the_content() ?>

                    <div class="contact-info">
                        <?php
                            $mobileNo = get_field('team_mob_no');
                            $emailAddress = get_field('team_email');
                        ?>

                        <?php echo ($mobileNo) ? '<span>Mobile: <a href="tel:'.str_replace(' ', '', $mobileNo).'">'.$mobileNo.'</a></span>' : '' ?>
                        <?php echo ($emailAddress) ? '<span>Email: <a href="mailto:'.$emailAddress.'">'.$emailAddress.'</a></span>' : '' ?>
                    </div>

                    <div class="social-links">
                        <?php
                            $linkedinProfile = get_field('team_linkedin_link');
                        ?>

                        <?php echo ($linkedinProfile) ? '<a class="linkedin-link" href="'.$linkedinProfile.'" target="_blank"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x fa-inverse"></i></span></a>' : '' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile ?>


<?php

    $args = [
        'post_type'    => 'meet-the-team',
        'post__not_in' => [get_the_ID()],
        'order'        => 'asc',
    ];

    $teamMembers = new WP_Query( $args );

?>

<style>
    @media(max-width: 479px) {
        .col-xxs-12 {
            width: 100% !important;
        }
    }
    .team-images-wrapper {
        max-width: 1140px;
        margin: 0 auto;
    }

    .team-images-wrapper.bottom {
        padding-top: 2rem;
        border-top: 1px solid #ddd;
    }

    .team-member {
        display: inline-block;
        position: relative;
        margin-bottom: 20px;
    }

    .team-member:before {
        opacity: 0;
        content: attr(title);
        margin-bottom: -30px;
        position: absolute;
        bottom: 120px;
        font-size: 30px;
        font-weight: normal;
        width: 100%;
        text-align: center;
        color: #FFF;
        z-index: 1;
        text-shadow: 0 0 15px #999;
        transition: margin-bottom 0.5s ease, opacity 0.5s ease;
    }

    .team-member:hover:before {
        margin-bottom: 0;
        opacity: 1;
    }

    .team-member:after {
        opacity: 0;
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(187, 199, 191, 0.85);
        transition: opacity 0.5s ease;
    }

    .team-member:hover:after {
        opacity: 1;
    }
</style>

<?php if ($teamMembers->have_posts()): ?>
<div class="team-images-wrapper bottom">
    <div class="row">
        <?php while ($teamMembers->have_posts()): $teamMembers->the_post() ?>
            <?php 
                $images = get_field('team_imgs', $teamMembers->ID);
                $featuredImage = array_shift($images);
            ?>
            <div class="col-xxs-12 col-xs-6 col-sm-4" data-aos="fade">
                <a href="<?php the_permalink($teamMembers->ID) ?>" class="team-member" title="<?php the_title() ?>">
                    <img class="img-responsive" src="<?php echo $featuredImage['url'] ?>">
                </a>
            </div>
        <?php endwhile ?>
    </div>
</div>
<?php wp_reset_postdata() ?>
<?php endif ?>

<?php get_footer(); ?>