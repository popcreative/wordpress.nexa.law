<?php get_header(); ?>

<style>
    @media(max-width: 479px) {
        .col-xxs-12 {
            width: 100% !important;
        }
    }
    .team-images-wrapper {
        max-width: 1140px;
        margin: 0 auto;
    }

    .team-member {
        display: inline-block;
        position: relative;
        margin-bottom: 20px;
    }

    .team-member:before {
        opacity: 0;
        content: attr(title);
        margin-bottom: -30px;
        position: absolute;
        bottom: 120px;
        font-size: 30px;
        font-weight: normal;
        width: 100%;
        text-align: center;
        color: #FFF;
        z-index: 1;
        text-shadow: 0 0 15px #999;
        transition: margin-bottom 0.5s ease, opacity 0.5s ease;
    }

    .team-member:hover:before {
        margin-bottom: 0;
        opacity: 1;
    }

    .team-member:after {
        opacity: 0;
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(187, 199, 191, 0.85);
        transition: opacity 0.5s ease;
    }

    .team-member:hover:after {
        opacity: 1;
    }
</style>

<div class="row" id="title">
    <div class="col-xs-12 col-md-5">
        <h1>Team</h1>
    </div>
    <div class="col-xs-12 col-md-7 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>

<div class="component border-top">
    <div class="team-images-wrapper">
        <div class="row">
            <?php 
                $teamPages = get_posts(array('post_type' => 'meet-the-team','order' => 'ASC'));
                $delay = 1;
            ?>
            <?php foreach($teamPages as $teamMember): ?>
                <div class="col-xxs-12 col-xs-6 col-sm-4" data-aos="fade" data-aos-delay="<?php echo $delay ?>00">
                    <?php 
                        $images = get_field('team_imgs', $teamMember->ID);
                        $featuredImage = array_shift($images);
                    ?>
                    
                    <a href="<?php the_permalink($teamMember->ID) ?>" class="team-member" title="<?php echo $teamMember->post_title ?>">
                        <img class="img-responsive" src="<?php echo $featuredImage['url'] ?>">
                    </a>
                </div>
                <?php $delay++ ?>
            <?php endforeach ?>
        </div>
    </div>
</div>

<div class="text-center">
    <?php posts_nav_link() ?>
</div>

<?php get_footer(); ?>