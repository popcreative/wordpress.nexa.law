<div class="calculator">
    <div class="slider">
        <div class="track">
            <input type="range" name="bill" value="50000" step="50000" min="50000" max="600000">
        </div>

        <div class="labels">
            <label>&pound;50,000</label>
            <label>fees billed</label>
            <label>&pound;600,000+</label>
        </div>
    </div>

    <form action="calculator_submit" method="get" accept-charset="utf-8">
        <p>
            Based on <input disabled type="input" value="£50,000" name="billed"> fees billed, you would take home
        </p>
        <p class="take-home">
            &pound;<span>37,500</span>
        </p>
    </form>
</div>
