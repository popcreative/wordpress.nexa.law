<?php

// Template Name: Tailored Earning Calculator
get_header();

?>

<?php while(have_posts()): the_post() ?>

<div class="row" id="title">
    <div class="col-xs-12 col-md-6">
        <h1><?php the_title() ?></h1>
    </div>
    <div class="col-xs-12 col-md-6 hidden-xs hidden-sm">
        <ul class="list-inline">
            <?php
                wp_nav_menu(array(
                    'menu' => 'Secondary',
                    'fallback_cb' => false,
                    'items_wrap' => '%3$s',
                    'container' => false
                ))
            ?>
        </ul>
    </div>
</div>

<div class="component two_columns component_index_0">

    <div class="row">
        <div class="col-xs-12 col-md-5">
            <div class="left thick-blue-border-md">
                <?php the_content() ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-md-offset-1">
            <div class="right">

                <form action="#" method="post">

                    <?php

                        $isValid = false;
                        $isSubmitted = isset($_POST['form']);

                        if ($isSubmitted) {
                            $templater = new \kevdotbadger\Templater\Renderer;
                            $validator = new kevdotbadger\Validator\Validator;
                            $validator->addRule('first_name')
                                ->notEmpty(array('message' => "First Name must not be empty"));
                            $validator->addRule('last_name')
                                ->notEmpty(array('message' => "Last Name must not be empty"));
                            $validator->addRule('email')
                                ->isEmail(array('message' => "Email Address must be a valid email address"));

                            $isValid = $validator->validate($_POST);

                            if ($isValid) {
                                $mailer = new \kevdotbadger\StupidMailer\Mailer;
                                $mailer->setFrom(array('website@nexa.law' => 'Nexa Law'));
                                $mailer->setSubject("Tailored Earning Calculator Enquiry");
                                $mailer->setRecipients(
                                    array(
                                        'info@nexa.law' => 'Nexa Law',
                                        'eliot.hibbert@nexa.law' => 'Eliot Hibbert',
                                        'forms@popcreative.co.uk'  => 'Pop Creative',
                                        //'timord@popcreative.co.uk' => 'Tim Ord'
                                    )
                                );

                                $mailer->addContent(trim(stripslashes(
                                    $templater->fill("
                                    Hello,

                                    A user has filled in the tailored earning calculator form, their details are below:

                                    First Name: {first_name}\n
                                    Last Name: {last_name}\n
                                    Email: {email}\n
                                    Hours a day they would you like to work: {hours}\n
                                    What they currently bill per year: {annum}\n
                                    How much they expect to charge per hour: {charge}
                                    ")->with($_POST)->render()
                                )));

                                $mailer->send();

                                // Send to Zoho
                                $client = new \Kevdotbadger\Zoho\Lead('e3ad0bd5da28020375bfeb51be08e3a9', 'eu');
                                $payload = [
                                    'Lead Source'               => 'Web Lead - Tailored Earning Calculator Form',
                                    'Lead Status'               => 'Open',
                                    'First Name'                => \kevdotbadger\Input\Input::get('first_name'), // needed
                                    'Last Name'                 => \kevdotbadger\Input\Input::get('last_name'), // needed
                                    'Email'                     => \kevdotbadger\Input\Input::get('email'),
                                    'Preferred working hours'   => \kevdotbadger\Input\Input::get('hours'),
                                    'Currently bill per year'   => \kevdotbadger\Input\Input::get('annum'),
                                    'Expect to charge per hour' => \kevdotbadger\Input\Input::get('charge')
                                ];
                                $client->create($payload);

                            }
                        }
                    ?>

                    <?php if ($isSubmitted) : ?>

                        <?php if ($isValid) : ?>

                            <div class="alert alert-success">
                                <p><i class="fa fa-check-circle" aria-hidden="true"></i> Thank you, the request has been sent to Nexa Law.</p>
                            </div>

                        <?php else : ?>

                            <div class="alert alert-danger">
                                <p><i class="fa fa-times-circle" aria-hidden="true"></i> We're sorry but the form failed to send. Please try again.</p>
                                <ul>
                                    <?php foreach ($validator->getErrorMessages() as $error) : ?>
                                        <li><?php echo $error ?></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>

                        <?php endif ?>

                    <?php endif ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name">First Name<sup>*</sup></label>
                                <input type="text" name="first_name" placeholder="First Name" class="form-control" required id="first_name">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="last_name">Last Name<sup>*</sup></label>
                                <input type="text" name="last_name" placeholder="Last Name" class="form-control" required id="last_name">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email">Email Address<sup>*</sup></label>
                        <input type="text" name="email" placeholder="Email Address" class="form-control" required id="email">
                    </div>

                    <div class="form-group">
                        <label for="hours">How many hours a day would you like to work per week?</label>
                        <input type="text" name="hours" placeholder="How many hours a day would you like to work per week?" class="form-control" id="hours">
                    </div>

                    <div class="form-group">
                        <label for="annum">What do you currently bill per annum?</label>
                        <input type="text" name="annum" placeholder="What do you currently bill per annum?" class="form-control" id="annum">
                    </div>

                    <div class="form-group">
                        <label for="charge">How much would you expect to charge per hour?</label>
                        <input type="text" name="charge" placeholder="How much would you expect to charge per hour?" class="form-control" id="charge">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-blue btn-block btn-wide">Send Me A Tailored Calculation</button>
                    </div>

                    <input type="hidden" name="form" value="form">

                </form>

            </div>
        </div>
    </div>
</div>


<?php endwhile ?>

<?php get_footer(); ?>