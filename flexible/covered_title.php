</div>
    <div class="container-grey">
        <div class="container">
            <div class="component grey
                <?php print make_component_class($content_index) ?>
            ">

            <?php

                $large_image = get_sub_field('large_image');
                $medium_image = get_sub_field('medium_image');
                $small_image = get_sub_field('small_image');

            ?>

            <style>

                .<?php print make_component_class($content_index) ?> {
                    background: #f5f5f5;
                    padding-top: 0;
                    padding-bottom: 0
                }

                .<?php print make_component_class($content_index) ?> .row {
                    display: flex;
                }

                .<?php print make_component_class($content_index) ?> .content {
                    padding-top: 2rem;
                    padding-bottom: 2rem;
                }

                .<?php print make_component_class($content_index) ?> .image {
                    display: flex;
                }

                .<?php print make_component_class($content_index) ?> .image div {
                    background: url(<?php echo $large_image['url'] ?>) no-repeat top right;
                    background-size: auto 100%;
                    flex: 1;
                    margin-left: 15px;
                }

                @media (max-width: 1200px) {
                    .<?php print make_component_class($content_index) ?> .content {
                        background: url(<?php echo $medium_image['url'] ?>) no-repeat top left;
                        background-size: cover;
                        width: 100%;
                    }

                    .<?php print make_component_class($content_index) ?> .image {
                        display: none;
                    }
                }

                @media (max-width: 768px) {
                    .<?php print make_component_class($content_index) ?> .content {
                        background: url(<?php echo $small_image['url'] ?>) no-repeat top left;
                        background-size: cover;
                    }
                }

                body.page-39 .<?php print make_component_class($content_index) ?> .content .lead {
                    font-size: 22px;
                }

                @media (min-width: 1200px) and (max-width: 1400px) {
                    body.page-41 .<?php print make_component_class($content_index) ?> .content .lead,
                    body.page-39 .<?php print make_component_class($content_index) ?> .content .lead {
                        font-size: 19px;
                    }
                }
            </style>

            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 content">
                    <div>
                        <?php the_sub_field('content') ?>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-6 col-md-6 image hidden-xs hidden-sm">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">