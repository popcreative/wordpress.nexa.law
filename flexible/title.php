<?php if (get_sub_field('grey_background')): ?>
</div>
<div class="container-grey">
<div class="container">
<?php endif ?>

<div class="component title
    <?php print get_sub_field('border_top') ? 'border-top':'' ?>
    <?php print get_sub_field('border_bottom') ? 'border-bottom':'' ?>
    <?php print 'component_index_'.$content_index ?>
">
    <div class="row">
        <div class="col-xs-12">
            <<?php the_sub_field('header_level') ?> class="<?php print (get_sub_field('is_green')) ? 'green' : '' ?>">
                <?php the_sub_field('title') ?>
            </<?php the_sub_field('header_level') ?>>
        </div>
    </div>
</div>

<?php if (get_sub_field('grey_background')): ?>
</div>
</div>
<div class="container">
<?php endif ?>