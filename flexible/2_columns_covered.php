<?php if (get_sub_field('grey_background')): ?>
</div>
<div class="container-grey">
<div class="container">
<?php endif ?>

<div class="component covered two_columns_covered
    <?php print get_sub_field('border_top') ? 'border-top':'' ?>
    <?php print get_sub_field('border_bottom') ? 'border-bottom':'' ?>
    <?php print get_sub_field('border_between') ? 'border-between':'' ?>
    <?php print get_sub_field('inverted') ? 'inverted':'' ?>
    <?php print make_component_class($content_index) ?>
">

    <style>
        <?php
            print str_replace(
                'column',
                "." . make_component_class($content_index) . " .left",
                get_sub_field('left_style')
            );
            print str_replace(
                'column',
                "." . make_component_class($content_index) . " .right",
                get_sub_field('right_style')
            );
        ?>
    </style>

    <div class="row">

        <?php $image = get_sub_field('image') ?>

        <?php if (get_sub_field('image_on') == 'Left') : ?>

            <div class="<?php print make_column_classes(get_sub_field('left_width')) ?>
                hidden-xs
                hidden-sm
                image
            ">
                <div style="background-image: url(<?php echo $image['url'] ?>)"></div>
            </div>
            <div class="<?php print make_column_classes(get_sub_field('right_width')) ?>">
                <div class="right">
                    <?php the_sub_field('content') ?>
                </div>
            </div>

        <?php else : ?>

            <div class="<?php print make_column_classes(get_sub_field('left_width')) ?>">
                <div class="left">
                    <?php the_sub_field('content') ?>
                </div>
            </div>
            <div class="<?php print make_column_classes(get_sub_field('right_width')) ?>
                hidden-xs
                hidden-sm
                image
            ">
                <div style="background-image: url(<?php echo $image['url'] ?>)"></div>
            </div>

        <?php endif ?>

    </div>
</div>

<?php if (get_sub_field('grey_background')): ?>
</div>
</div>
<div class="container">
<?php endif ?>