<?php if (get_sub_field('grey_background')): ?>
</div>
<div class="container-grey">
<div class="container">
<?php endif ?>

<div class="component one_column
    <?php print get_sub_field('border_top') ? 'border-top':'' ?>
    <?php print get_sub_field('border_bottom') ? 'border-bottom':'' ?>
    <?php print make_component_class($content_index) ?>
">

    <style>
        <?php
            print str_replace(
                'column',
                "." . make_component_class($content_index) . " .row > div > div",
                get_sub_field('style')
            );
        ?>
    </style>

    <div class="row">
        <div class="col-xs-12">
            <div>
                <?php the_sub_field('content') ?>
            </div>
        </div>
    </div>
</div>

<?php if (get_sub_field('grey_background')): ?>
</div>
</div>
<div class="container">
<?php endif ?>