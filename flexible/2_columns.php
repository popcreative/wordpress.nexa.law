<?php if (get_sub_field('grey_background')): ?>
</div>
<div class="container-grey">
<div class="container">
<?php endif ?>

<div class="component two_columns
    <?php print get_sub_field('border_top') ? 'border-top':'' ?>
    <?php print get_sub_field('border_bottom') ? 'border-bottom':'' ?>
    <?php print get_sub_field('border_between') ? 'border-between':'' ?>
    <?php print make_component_class($content_index) ?>
">

    <style>
        <?php
            print str_replace(
                'column',
                "." . make_component_class($content_index) . " .left",
                get_sub_field('left_style')
            );

            print str_replace(
                'column',
                "." . make_component_class($content_index) . " .right",
                get_sub_field('right_style')
            );
        ?>
    </style>

    <div class="row">
        <div class="<?php print make_column_classes(get_sub_field('left_width')) ?>">
            <div class='left'>
                <?php the_sub_field('left_content') ?>
            </div>
        </div>
        <div class="<?php print make_column_classes(get_sub_field('right_width')) ?>">
            <div class='right'>
                <?php the_sub_field('right_content') ?>
            </div>
        </div>
    </div>
</div>

<?php if (get_sub_field('grey_background')): ?>
</div>
</div>
<div class="container">
<?php endif ?>